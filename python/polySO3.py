from scipy.linalg import expm
import numpy as np

def mulSO3(p,q):
    """
    Multiply rotations in SO3, using axis angle representation.
    This is essentially just doing unit quaternion multiplication
    """

def expSO3(v):
    """
    Rodrigues rotation formula
    """

def logSO3(R):
    """
    Inverse Rodrigues
    """
    theta = np.arccos(0.5*(R.trace()[0,0]-1))
    return invstarmap((R-R.T)*(theta/(2.*np.sin(theta))))

def invstarmap(S):
    A = 0.5*np.array([[S[2,1]-S[1,2]], \
                      [S[0,2]-S[2,0]], \
                      [S[1,0]-S[0,1]]])
    return A

def starmap(v):
    A = np.array([[0,-v[2],v[1]],
                  [v[2],0,-v[0]],
                  [-v[1],v[0],0]]);
    return np.matrix(A)

def integrateSO3PolynomialBiinvariant(R0,xi0,Nt,dt):
    """
    Integrate a polynomial in SO(3) using the given initial conditions
    """

    k = xi0.shape[1]

    R = [R0];
    xi = [xi0];

    for i in xrange(Nt):
        xit = xi[i].copy()
        # shorthand for velocity
        v = np.squeeze(xit[:,0].copy())
        # take euler step in rotation
        R.append( (np.matrix(R[i]*expm(dt*starmap(xit[:,0])))).copy() )

        for j in xrange(k):
            xit[:,j] += 0.5*dt*np.matrix(np.cross(v,np.squeeze(xit[:,j]))).T
            if j < (k-1):
                xit[:,j] += dt*(xit[:,j+1])
        xi.append( xit )

    return (xi,R)
