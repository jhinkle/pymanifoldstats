import numpy as np
import scipy

#import pylab as plt
##import matplotlib.axes3d as p3
#import mpl_toolkits.mplot3d.axes3d as p3
#plt.ion()

from mayavi import mlab
from mayavi.tools import server
mlab.options.background_color = (1,1,1)

import polySO3
reload(polySO3)

if __name__ == '__main__':
    # set up integration
    T = 1.25 # time to integrate
    Nt = 1001 # num time steps
    dt = T/Nt

    basepoints = np.matrix([[0,-1.,0]]).T
            #,[0,-.5,np.sqrt(3)/2.],[0,-.5,-np.sqrt(3)/2.]]).T # we could do multiple basepoints if we want

    # set up initial conditions
    #xi0 = np.matrix([[0,0,1.5],     # velocity
                 #[-.3,-.5,0],     # acceleration
                 #[2,.5,-1.5]]).T # jerk
    # match ECCV
    xi0 = np.matrix([[0,0,np.sqrt(2)],     # velocity
                 [-2.5,0,0],     # acceleration
                 [9,0,-np.sqrt(2)]]).T # jerk

    k = 3 # order of polynomial to run

    # plot params
    lw = 3 # line width
    #colors = ['k','b','r']
    colors = [(0,0,0),(0,0,1),(1,0,0)]
    plotSphere = True
    # camera params
    #defelev = 15.62622504930965
    #defazim = -34.152038515028266
    defelev = 75
    defazim = -50

    # integrate polynomial
    [xit,Rt] = polySO3.integrateSO3PolynomialBiinvariant(np.matrix(np.eye(3)),xi0[:,0:k],Nt,dt)

    # plot rotating axes
    plotAxesEvery = 100
    fig = mlab.figure('axes')
    mlab.clf()
    minOpac=0.05
    maxOpac=1.0
    timeexp=1
    tuberad = 0.007
    for jj in xrange(0,Nt,plotAxesEvery):
        R = Rt[jj]
        normtime = float(jj)/float(Nt)
        normtime = 1.0 - normtime # reverse fade
        normtime = normtime**timeexp
        opac = minOpac + (maxOpac-minOpac)*normtime
        sxi = np.matrix([[0,0,0.]]).T
        #mlab.quiver3d(sxi[0,0],sxi[0,0],sxi[0,0],R[0,0],R[1,0],R[2,0],line_width=lw,color=(.1,.5,.5),scale_mode='scalar',scale_factor=3.)
        #mlab.quiver3d(sxi[0,0],sxi[0,0],sxi[0,0],R[0,1],R[1,1],R[2,1],line_width=lw,color=(.5,.5,.1),scale_mode='scalar',scale_factor=3.)
        #mlab.quiver3d(sxi[0,0],sxi[0,0],sxi[0,0],R[0,2],R[1,2],R[2,2],line_width=lw,color=(.5,.1,.5),scale_mode='scalar',scale_factor=3.)
        mlab.plot3d([0,R[0,0]],[0,R[1,0]],[0,R[2,0]],line_width=lw,tube_radius=tuberad,color=(.1,.5,.8),opacity=opac)
        mlab.plot3d([0,-R[0,1]],[0,-R[1,1]],[0,-R[2,1]],line_width=lw,tube_radius=tuberad,color=(1.,.7,.1),opacity=opac)
        mlab.plot3d([0,R[0,2]],[0,R[1,2]],[0,R[2,2]],line_width=lw,tube_radius=tuberad,color=(.0,.7,.0),opacity=opac)
    mlab.view(azimuth=defazim,elevation=defelev,distance=8,focalpoint=(0,0,0.5))
    # output
    mlab.savefig('rotaxisOrder'+str(k)+'.png')

    # set up figure
    #fig = plt.figure('trajectories')
    #plt.clf()
    fig = mlab.figure('trajectories')
    mlab.clf()

    #ax = fig.gca(projection='3d')

    # act on points and produce figures

    for bp in xrange(basepoints.shape[1]):
        rotaxis = np.matrix([[0.],[0.],[0.]])
        pt = basepoints[:,bp:bp+1].copy()
        for ii in xrange(1,len(Rt)): # get point using group action
            pt = np.append(pt,Rt[ii]*basepoints[:,bp],axis=1)
            rotaxis = np.append(rotaxis,polySO3.logSO3(Rt[ii]),axis=1)
        pt = np.array(pt)
        rotaxis = np.array(rotaxis)

        # plot moving points, axis
        #ax.plot(pt[0,0:1],pt[1,0:1],pt[2,0:1],'.k',markersize=10)
        #plt.hold(True)
        #ax.plot(pt[0,:],pt[1,:],pt[2,:],colors[k-1])

        mlab.points3d(pt[0,0:1],pt[1,0:1],pt[2,0:1],color=(0,0,0),scale_factor=.05)
        mlab.plot3d(pt[0,:],pt[1,:],pt[2,:],color=colors[k-1],line_width=lw,tube_radius=None)
        mlab.plot3d(pt[0,:],pt[1,:],pt[2,:],color=(0,0,0),line_width=lw,tube_radius=None)

        # plot rotation axis
        #mlab.plot3d(rotaxis[0,:],rotaxis[1,:],rotaxis[2,:],color=(.0,.5,.0),line_width=lw,tube_radius=None)

        for i in xrange(k):
            xiit = xi0[:,i]
            xii = 1.5*xiit/scipy.linalg.norm(xiit)

            #if i == 0:
                #sxi = np.matrix([[0,0,0.]]).T
                #xii = xi0[:,i]
                #mlab.plot3d([sxi[0,0],xii[0]],[sxi[1,0],xii[1]],[sxi[2,0],xii[2]],line_width=lw,color=colors[i],tube_radius=None)
            #else:
            if True:
                #sxi = xi0[:,0]
                sxi = np.matrix([[0,0,0.]]).T
                xii = xi0[:,i]# + 0.02*xi0[:,0]#/float(i)
                mlab.quiver3d(sxi[0,0], sxi[1,0], sxi[2,0], xii[0,0],xii[1,0],xii[2,0],line_width=lw,color=colors[i],scale_mode='scalar',scale_factor=scipy.linalg.norm(xii)*.2)
                #mlab.plot3d([sxi[0,0],xii[0]],[sxi[1,0],xii[1]],[sxi[2,0],xii[2]],line_width=lw,color=colors[i],tube_radius=None)
                #ax.plot([sxi[0,0],xii[0]],[sxi[1,0],xii[1]],[sxi[2,0],xii[2]],'--'+colors[i],linewidth=.5*lw)
                #ax.arrow(xi0[0,0],xi0[1,0],xi0[2,0],xii[0],xii[1],xii[2],colors[i])
            # also plot the movement of the axis-angle representation of xi[i]

            if i == -1:
                for ii in xrange(1,len(Rt)): # get point using group action
                    xiit = xit[ii][:,i]
                    #xiit = 1.5*xiit/scipy.linalg.norm(xiit)
                    xii = np.append(xii,xiit,axis=1)
                xii = np.array(xii)
                #ax.plot(xii[0,:],xii[1,:],xii[2,:],'--'+colors[i],linewidth=lw)
                mlab.plot3d(xii[0,:],xii[1,:],xii[2,:],line_width=lw,color=colors[i],tube_radius=None)


        ## poor man's way to set axis equal (you need to zoom in afterward)
        #r=5.
        #ax.plot([r],[r],[r],'.',alpha=0.)
        #ax.plot([-r],[r],[r],'.',alpha=0.)
        #ax.plot([r],[-r],[r],'.',alpha=0.)
        #ax.plot([-r],[-r],[r],'.',alpha=0.)
        #ax.plot([r],[r],[-r],'.',alpha=0.)
        #ax.plot([-r],[r],[-r],'.',alpha=0.)
        #ax.plot([r],[-r],[-r],'.',alpha=0.)
        #ax.plot([-r],[-r],[-r],'.',alpha=0.)

    if plotSphere:
        # Create a sphere
        r = 1.0
        pi = np.pi
        cos = np.cos
        sin = np.sin
        phi, theta = np.mgrid[0:pi:25j, 0:2*pi:25j]

        x = r*sin(phi)*cos(theta)
        y = r*sin(phi)*sin(theta)
        z = r*cos(phi)
        #ax.plot_surface(x, y, z,  rstride=2, cstride=2, linewidth=.01, alpha=.01, antialiased=True, color='#FFFFFF')
        #s = mlab.mesh(x,y,z,opacity=.8,color=(.9,.9,.9),representation='surface')
        s = mlab.mesh(x,y,z,opacity=.2,color=(1,1,1),representation='wireframe',line_width=0.01,tube_radius=None)


    #plt.axis('equal')
    #ax.view_init(elev=defelev,azim=defazim)
    #plt.axis('off')
    #plt.draw()

    mlab.view(azimuth=defazim,elevation=defelev,distance=8,focalpoint=(0,0,0.5))
    
    ##save figure
    #plt.savefig('trajectoriesOrder'+str(k)+'.pdf')
    mlab.savefig('pointTrajectoriesOrder'+str(k)+'.png')

    #server.serve_tcp()
    mlab.show()
